<?php

/**
 * @file
 * Contains Drupal\Tests\video_embed_sketchfab\Unit\ProviderUrlParseTest.
 */

namespace Drupal\Tests\video_embed_sketchfab\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\video_embed_sketchfab\Plugin\video_embed_field\Provider\Sketchfab;

/**
 * Test that URL parsing for the provider is functioning.
 *
 * @group video_embed_sketchfab
 */
class ProviderUrlParseTest extends UnitTestCase {

  /**
   * @dataProvider urlsWithExpectedIds
   *
   * Test URL parsing works as expected.
   */
  public function testUrlParsing($url, $expected) {
    $this->assertEquals($expected, Sketchfab::getIdFromInput($url));
  }

  /**
   * A data provider for URL parsing test cases.
   *
   * @return array
   *   An array of test cases.
   */
  public function urlsWithExpectedIds() {
    return [
      [
        'https://sketchfab.com/models/9f793b1a6fdd470d9f18c2c92ff7c0e6',
        '9f793b1a6fdd470d9f18c2c92ff7c0e6',
      ],
      [
        'http://sketchfab.com/models/9f793b1a6fdd470d9f18c2c92ff7c0e6',
        '9f793b1a6fdd470d9f18c2c92ff7c0e6',
      ],
      [
        'https://sketchfab.com/models/9f793b1a6fdd470d9f18c2c92ff7c0e6/',
        '9f793b1a6fdd470d9f18c2c92ff7c0e6',
      ],
      [
        'http://sketchfab.com/models/9f793b1a6fdd470d9f18c2c92ff7c0e6/',
        '9f793b1a6fdd470d9f18c2c92ff7c0e6',
      ],
      [
        'https://www.sketchfab.com/9f793b1a6fdd470d9f18c2c92ff7c0e6',
        FALSE,
      ],
      [
        'https://www.sketchfab.com/viralthreaddotcom/not/9f793b1a6fdd470d9f18c2c92ff7c0e6',
        FALSE,
      ],
      [
        'https://www.sketchfab.com/videos/9f793b1a6fdd470d9f18c2c92ff7c0e6',
        FALSE,
      ],
      [
        'https://www.sketchfab.com/9f793b1a6fdd470d9f18c2c92ff7c0e6/',
        FALSE,
      ],
    ];
  }
}
